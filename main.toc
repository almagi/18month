\select@language {english}
\contentsline {chapter}{Abstract}{i}
\contentsline {chapter}{Acknowledgements}{iii}
\contentsline {chapter}{Declaration of Authorship}{v}
\contentsline {chapter}{List of Figures}{ix}
\contentsline {chapter}{List of Tables}{x}
\contentsline {chapter}{List of Symbols}{xi}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Motivation}{1}
\contentsline {section}{\numberline {1.2}Overview and Objectives}{1}
\contentsline {chapter}{\numberline {2}Scientific Background}{3}
\contentsline {section}{\numberline {2.1}Historical Context}{3}
\contentsline {section}{\numberline {2.2}Shock wave/Boundary layer Interactions}{4}
\contentsline {subsection}{\numberline {2.2.1}Normal SBLI}{5}
\contentsline {subsection}{\numberline {2.2.2}Oblique SBLI}{5}
\contentsline {section}{\numberline {2.3}Sidewall Influence}{7}
\contentsline {subsection}{\numberline {2.3.1}Swept SBLI}{7}
\contentsline {subsection}{\numberline {2.3.2}Corner Flow}{8}
\contentsline {section}{\numberline {2.4}Shock Strength Influence}{10}
\contentsline {subsection}{\numberline {2.4.1}Mach Reflections and Irregular SBLI}{10}
\contentsline {subsection}{\numberline {2.4.2}Effect on Flow Separation}{14}
\contentsline {section}{\numberline {2.5}Shock Trains}{15}
\contentsline {section}{\numberline {2.6}Summary}{17}
\contentsline {chapter}{\numberline {3}Computational Setup}{18}
\contentsline {section}{\numberline {3.1}OpenSBLI}{18}
\contentsline {section}{\numberline {3.2}Governing Equations}{19}
\contentsline {section}{\numberline {3.3}Numerical Schemes}{20}
\contentsline {subsection}{\numberline {3.3.1}Shock Capturing}{20}
\contentsline {subsection}{\numberline {3.3.2}Adaptive Schemes}{22}
\contentsline {subsection}{\numberline {3.3.3}Temporal Discretisation}{23}
\contentsline {section}{\numberline {3.4}Computational Domain}{24}
\contentsline {subsection}{\numberline {3.4.1}Grid}{24}
\contentsline {subsection}{\numberline {3.4.2}Boundary Conditions}{25}
\contentsline {section}{\numberline {3.5}Turbulence Generation}{26}
\contentsline {subsection}{\numberline {3.5.1}Overview}{26}
\contentsline {subsection}{\numberline {3.5.2}Mean Profile Generation}{28}
\contentsline {subsection}{\numberline {3.5.3}Fluctuating Profile Generation}{30}
\contentsline {subsubsection}{Original Method}{30}
\contentsline {subsubsection}{Modifications for Current Project}{32}
\contentsline {subsection}{\numberline {3.5.4}Adaptation for Sidewalls}{33}
\contentsline {chapter}{\numberline {4}Results}{36}
\contentsline {section}{\numberline {4.1}Turbulent Boundary Layer}{36}
\contentsline {subsection}{\numberline {4.1.1}Validation}{36}
\contentsline {subsection}{\numberline {4.1.2}Numerical Schemes}{40}
\contentsline {subsection}{\numberline {4.1.3}Grid Dependence}{40}
\contentsline {section}{\numberline {4.2}Constant Area Duct}{43}
\contentsline {section}{\numberline {4.3}SBLI Exploratory Studies}{47}
\contentsline {subsection}{\numberline {4.3.1}Span Periodic Cases}{47}
\contentsline {subsection}{\numberline {4.3.2}Sidewall Case}{48}
\contentsline {subsection}{\numberline {4.3.3}Strong Interaction Cases}{51}
\contentsline {section}{\numberline {4.4}SBLI Validation}{54}
\contentsline {chapter}{\numberline {5}Future Work}{58}
\contentsline {section}{\numberline {5.1}SBLI cases}{58}
\contentsline {section}{\numberline {5.2}Constant Area Duct Cases}{58}
\contentsline {section}{\numberline {5.3}Miscelaneous}{59}
\contentsline {chapter}{References}{61}
