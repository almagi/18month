\chapter{Introduction}
\label{chap:intro}

	\section{Motivation}
		\label{sec:motivation}
		
		A shock wave/boundary layer interaction (SBLI) is a common phenomenon experienced in high-speed flight, occurring in both internal and external flows at speeds ranging from transonic to hypersonic. In brief, a SBLI is any situation in which a shock wave encounters (or is borne out of) a boundary layer. Therefore any interaction between a shock-wave and a solid surface will create an SBLI of some description.\\
		
		\noindent SBLIs are of interest primarily due to the problems that they cause in regards to high-speed vehicles. In general, the highest thermal and pressure loading on the vehicle structure is found in SBLI regions \cite{dolling2001} meaning there are huge considerations for the structural design and surface materials (i.e. weight and cost) wherever SBLI are expected to be found.\\
		
		\noindent There is particular interest in SBLI behaviour in supersonic compression intakes (see figure \ref{fig:intakes}) which use a series of ramp-generated oblique shock waves to decelerate the flow before the subsonic compressor. Every time there is a shock reflection in the intake there is an SBLI and each of those interactions is associated with a large adverse pressure gradient and an increase in viscous dissipation leading to, at best, a thickening of the post-shock boundary layer and, at worst, full flow separation. In these cases, the motion of SBLIs in supersonic intakes is highly sensitive to upstream conditions and to self-induced oscillations \cite{piponniau2009}. The entire shock system can oscillate in the streamwise direction, and, in the worst-case scenario, move upstream out of the compressor, forming a bow shock (this is known as ``unstart'' and can lead to total loss of control of an aircraft).\\
		
		\begin{figure}[h]
			\centering
			\begin{subfigure}[b]{0.45\textwidth}
				\centering
				\includegraphics[width=\textwidth]{concorde.jpeg}
				\label{fig:concorde}
			\end{subfigure}
			\hfill
			\begin{subfigure}[b]{0.532\textwidth}
				\centering
				\includegraphics[width=\textwidth]{tornado.jpg}
				\label{fig:tornado}
			\end{subfigure}
			\caption[Photographs of mixed compression intakes]{Photographs of the mixed compression intakes onboard the Concorde (left, \cite{concorde-photo}) and Panavia Tornado (right, \cite{tornado-photo}).}
			\label{fig:intakes}
		\end{figure}
			
		\noindent All of these problems have consequences for the performance of the vehicle and hence a better understanding of the SBLI problem allows engineers to design aircraft, rockets and missiles to achieve higher efficiencies for a wide range of operating conditions. This is the main motivation behind research in this field.\\
		
	\section{Overview and Objectives}
		\label{sec:overview}
		
		\noindent This report provides a summary of the work undertaken by the author in the first 18 months of a research project focusing on the behaviour of SBLIs in 3D ducts, specifically, the on the effects of shock strength and sidewalls. The scientific background on the various types of SBLI are discussed in chapter \ref{chap:lit}. Additionally we provide an up to date review of the literature in the area, revealing a number of likely sources of novel research. Chapter \ref{chapter:numerical} provides a thorough description of the computational arrangement, including the finite difference solver, the governing equations and numerical formulation, and the various grid descriptions and boundary conditions. The last section in chapter \ref{chapter:numerical} is devoted to a full description of the method for generating turbulent boundary layers, including modifications made for the current project.\\
		
		\noindent The interim results are presented in chapter \ref{chap:results}, including an assessment of the turbulence generation method with a flat plate boundary layer case; the initial results looking at flow within square ducts; a number of exploratory test cases looking at SBLIs; and finally, up to date results on the ongoing SBLI validation study. The final chapter is concerned with plotting the course of the project over the next 18 months.\\
		
		\noindent The main objectives for the project as a whole are as follows:
		
		\begin{itemize}
			\item Studying the effects of sidewalls on the behaviour of SBLIs within 3D ducts.
			\item Studying the same sidewall effects in conjunction with variable shock strength.
			\item Developing an improved method of turbulent flow generation suitable for compressible boundary layers on multiple walls.
		\end{itemize}
		
		
		
		
		