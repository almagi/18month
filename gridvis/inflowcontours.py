import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.backends.backend_pdf import PdfPages
import removehalos
import h5py
from opensbli.utilities.turbulent_inflow.core import *

plt.style.use('classic')
font = {'size'   : 16}

matplotlib.rc('font', **font)


constants = ['gama', 'Minf', 'Pr', 'Re', 'Twall', 'dt', 'niter', 'block0np0', 'block0np1', 'block0np2', 'Lx0',
     'Lx1', 'Lx2', 'Delta0block0', 'Delta1block0', 'Delta2block0', 'SuthT', 'RefT', 'eps', 'TENO_CT', 'teno_a1', 'teno_a2', 'TENO_A','by', 'bz', 
     'restart_iteration_no', 'configuration', 'bp_factor', 'referenceRe', 'ducros']
values = ['1.4', '2.7', '0.72', '1800.0', '2.307', '0.004', '1000', '540', '300', '300', '375.0', '100.0', '100.0', 
    'Lx0/(block0np0-1)', 'Lx1/(block0np1-1)', 'Lx2/(block0np2-1)', '110.4', '273.0', '1e-15', '1e-5', '10.5', '4.5', '2.5','1.9', '1.9', 
    '0', 'corner', '1.0', '1000', '1.0']

inputs = get_all_vars(constants, values)
input_dictionary = dict(zip(constants, values))

umean, vmean, wmean, tmean, rhomean, A, seeds = inflow_init(constants, values)
A1 = A[:,:,0]
A21 = A[:,:,3]
A22 = A[:,:,1]
A22 = A[:,:,1]

umean = np.transpose(umean)
A1 = np.transpose(A1)
A21 = np.transpose(A21)
A22 = np.transpose(A22)

## Get reference data
# print "Importing reference data"
# try:
#     fname = 'turbulentinflow_pp.h5'
#     f = h5py.File(fname, 'r')
# except:
#     removehalos.strip_halos("turbulentinflow.h5", "turbulentinflow_pp.h5")
#     fname = 'turbulentinflow_pp.h5'
#     f = h5py.File(fname, 'r')
# group = f["opensbliblock00"]
# A1 = group["A11_B0"].value[:,:,0]
# umean = group["Umean_B0"].value[:,:,0]

## Get grid data
print "Importing grid data"
try:
    fname = 'grid_pp.h5'
    f = h5py.File(fname, 'r')
except:
    removehalos.strip_halos("grid.h5", "grid_pp.h5")
    fname = 'grid_pp.h5'
    f = h5py.File(fname, 'r')
group = f["opensbliblock00"]
x = group["gridx_B0"].value
y = group["gridy_B0"].value
z = group["gridz_B0"].value
nx = group["gridx_B0"].shape

h, c1 = [np.zeros((nx[0],nx[1])) for _ in range(2)]
blt = np.interp(0.99, umean[int(0.5*nx[0]),:], y[0, :, 0])
print blt

for k in range(nx[0]):
    for j in range(nx[1]):

        # get the distances to the nearest wall
        ydist = min(y[k,j,0], y[k,-1,0] - y[k,j,0])
        zdist = min(z[k,j,0], z[-1,j,0] - z[k,j,0])

        # corner parameter, h: used to determine weight parameters c1 and c2
        h[k,j] = ydist - zdist

        # outwith corner:
        if h[k,j] > blt:
            c1[k,j] = 0.0
        elif h[k,j] < -blt:
            c1[k,j] = 1.0
        else:
            # within corner region, compute weight parameters
            c1[k,j] = 0.5 - 0.5*np.sin(0.5*np.pi*h[k,j]/blt)

        c2 = 1.0 - c1[k,j]

        if zdist <= blt:
        	c1[k,j] *= (np.absolute(zdist)/blt)**0.5
        if ydist <= blt:
        	c2 *= (np.absolute(ydist)/blt)**0.5

lz, ly, lx = [int(0.4*n) for n in nx]

y = y[:lz,:ly,0]/blt
z = z[:lz,:ly,0]/blt

umean = umean[:lz,:ly]
A1 = A1[:lz,:ly]
A21 = A21[:lz,:ly]
A22 = A22[:lz,:ly]
h = h[:lz,:ly]
c1 = c1[:lz,:ly]

variables = []
labels = []
filenames = []

variables += [A1]
filenames += ["A11"]
labels += [r"$A_{11}$"]

# variables += [A21]
# labels += ["A21"]

# variables += [A22]
# labels += ["A22"]

variables += [umean]
filenames += ["umean"]
labels += [r"$\overline{u}$"]

# variables += [umean - A1]
# labels += ["diff"]

# variables += [h]
# labels += ["h"]

# variables += [c1]
# labels += ["c1"]
sepaerate_files = True
n_levels = 25
with PdfPages('inflow.pdf') as pdf1:
	plt.rc('font', family='serif')
	plt.rc('font', family='serif')
	for i, var in enumerate(variables):
		print "Generating contour: {}".format(i+1)
		fig = plt.figure()
		fig.set_size_inches(11.69,8.27)
		ax = fig.add_subplot(1,1,1, aspect='equal')
		var_min = np.min(var)
		var_max = np.max(var)
		levels = np.linspace(var_min, var_max, n_levels)
		lab = labels[i]

		CS = ax.contourf(z, y, var, cmap = cm.RdBu, levels=levels, alpha=1.0, nchunk=2)

		if "diff" in labels[i].lower():
			zero = ax.contour(z, y, var, levels=[0.0], colors='w', linewidths=1.0, linestyles='solid')

		#if "umean" in labels[i].lower():
		blt = ax.contour(z, y, umean, levels=[0.99], colors='w', linewidths=2.0, linestyles='solid')

		for c in CS.collections:
		    c.set_edgecolor("face")

		#ax.axhline(blt, linewidth=0.5, linestyle='solid', color='w')

		ticks_at = np.linspace(levels[0], levels[-1], 5)
		cbar = plt.colorbar(CS, ticks=ticks_at, format='%.3f')
		cbar.ax.set_ylabel(lab, fontname='serif', fontsize=24)
		plt.xlabel(r"$z/\delta_0$", fontname='serif', fontsize=24)
		plt.ylabel(r"$y/\delta_0$", fontname='serif', fontsize=24)
		if sepaerate_files:
			plt.savefig("{}_{}.png".format(filenames[i], input_dictionary["configuration"]), bbox_inches='tight', dpi=200)
		else:
			pdf1.savefig(bbox_inches='tight')

		plt.close()