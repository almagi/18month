'''
grid.py

Use this code to 3D shock generator grids

Main function:

    generate(theta, x1, x2, x3, y1, z1, nx, ny, nz, by, bz, configuration="bottom")

    - theta: flowdeflection angle in deg
    - x1, x2, x3: grid lengths separating corners
    - y1: height of grid at x=0
    - z1: grid width
    - nx, ny, nz: number of grid points
    - by, bz: stretching factors to resolve near-wall flow

'''

import math
import numpy as np
from numpy import tanh, sinh
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import h5py

nhaloleft = 5
nhaloright = 5


def polyCoefs(w, h, s1, s2):
    '''
    Function to find the polynomial coefficients of a curve connecting two straight lines
    
    Input:
    - x and y distance between points
    - gradients of each line
    
    Output:
    
    - coefficients of x, x**3, x**4, x**5 terms
    '''
    a2 = s1 # first coefficient allocated to initial gradient
    
    # The matrix equation F*a = x is solved here for a, which provides the remaining coefficients
    F = [[w**3, w**4, w**5], [3*w**2, 4*w**3, 5*w**4], [6*w, 12*w**2, 20*w**3]]
    G = np.linalg.inv(F)
    x = [[h - s1*w], [s2 - s1], [0]]
    
    [[a4], [a5], [a6]] = np.matmul(G, x)
    
    
    return a2, a4, a5, a6
    
      
def buildPoly(N, point1, point2):
    '''
    Function to build a polynomial curve between 2 given points
    
    Input:
    - number of data points on curve
    - point objects with coordinates and gradient
    
    Output:
    - X, Y data points
    '''
    N = int(N)
    
    # Retrive values from object inputs
    x1, y1, s1 = point1.getx(), point1.gety(), point1.getGrad()
    x2, y2, s2 = point2.getx(), point2.gety(), point2.getGrad()
    
    # x and y distances between points
    w = x2 - x1
    h = y2 - y1
    
    # Preallocated values
    X = np.linspace(x1, x2, N)
    Y = []
    
    # Y axis intercepts of both lines
    c1 = -s1*x1 + y1
    c2 = -s2*x2 + y2
    
    # get coefficients from polyCoefs function
    a2, a4, a5, a6 = polyCoefs(w, h, s1, s2)
    
    
    for i in range(N):
        x = X[i] - x1
        # Builds y data points based on polynomial expression
        y = y1 + a2*x + a4*x**3 + a5*x**4 + a6*x**5
        Y.append(y)
    
    return X, Y    
   
    
def allocateNx(Nx, points):
    '''
    Function to distribute data points between all 5 curves based on x distance between start/end
    
    Input:
    - total number of data points
    - list of start/end point objects
    
    Output:
    - List of data points for each line
    '''
    x = []
    y = []
    Nxi = []
    
    for i in range(len(points)):
        # builds arrays of x & y values from start/end points
        x.append(points[i].getx())
        y.append(points[i].gety())
        
    
    for i in range(4):
        # Number of points on each line allocated based on share of x distance
        Nxi.append(round(Nx * (x[i + 1] - x[i])/(x[-1] - x[0])))
    
    # To avoid rounding problems, final curve is allocated the remainder of dat points
    Nx5 = Nx - sum(Nxi)
    Nxi.append(Nx5)
    
    return Nxi


def yStretch(I, Ly, stretchFactor, Ny, configuration):
    '''
    Function to implement grid stretching
    
    Input:
    - current iteration
    - total grid height of current position
    - stetching factor
    - number of grid points in y
    
    Output:
    - height of current gridline
    '''
    if float(stretchFactor) == 0.0:
        # Exception for when stetch factor is zero (no stetching)
        return Ly*I/Ny
        
    else:
        if configuration in ["full", "bottom shock", "channel shock", "channel"]:
            return Ly*0.5*(1.0 - tanh(stretchFactor*(1.0 - 2.0*I/(Ny - 1.0)))/tanh(stretchFactor))
        else:
            # Implements the hyperbolic sine function used in openSBLI
            return Ly * sinh(stretchFactor *I /(Ny - 1.0))/sinh(stretchFactor)

def sanitiseInput(x, y, beta):

    '''
    Function to check that beta values are not too large and returns appropriate values if so
    
    Input:
    - Grid geometry
    - arc angles
    
    Output
    - new arc angles
    '''
    [x1, x2, x3] = x
    [y1, y2, y3, y4] = y
    beta = map(lambda b: b*math.pi/180, beta)
    [beta1, beta2, beta3, beta4] = beta
    
    # Threshold value sets the maximum length that the arc angle can sweep along the bottom surface
    threshold = 0.8
    
    l1 = y2 * math.tan(beta1)
    l2 = y2 * math.tan(-beta2)
    l3 = y3 * math.tan(beta3)
    l4 = y3 * math.tan(-beta4)
    
    if l1 > threshold*x1:
        beta1 = math.atan(threshold*x1/y2) 
    if (l2 + l3) > threshold*x2:
        beta2 = -math.atan(0.5*threshold*x2/y2)
        beta3 = math.atan(0.5*threshold*x2/y3)
    if l4 > threshold*x3:
        beta4 = -math.atan(threshold*x3/y3)    
    
    beta = [beta1, beta2, beta3, beta4]
    
    beta = map(lambda b: b*180/math.pi, beta)
    
    
    if beta[0] > -2*beta[1]:
        beta[0] = -2*beta[1]
    if beta[3] < -2*beta[2]:
        beta[3] = -2*beta[2]
    
    return beta
   
    
def getPoints(I, Ny, x, y, beta, stretchFactor, configuration):
    '''
    Function to find the start/end points of the interconnected lines. This is applied to each horizontal grid line.
    
    Input:
    - I, current iteration
    - Ny, total number of y grid points
    - Grid geometry (x: section lengths, y: corner heights)
    - interpolation arc angles in degrees
    - stretch factor in y direction
    
    Output:
    - 6x point objects
    
    '''
    # Input values stored in arrays - array position corresponds to each intermmediate point
    
    [x1, x2, x3] = x
    [y1, y2, y3, y4] = y
    
    x0 = [x1, x1, x2 + x1, x2 + x1]
    y0 = [y2, y2, y3, y3]
    
    points = []
    
    # Starting point at inlet
    points.append(point(0, yStretch(I, y1, stretchFactor, Ny, configuration)))
    
    # Helper points (grid points where straight sections intercept)
    hpoint1 = point(x1, yStretch(I, y2, stretchFactor, Ny, configuration))
    hpoint2 = point(x1 + x2, yStretch(I, y3, stretchFactor, Ny, configuration))
    
    # End point at outlet
    point6 = point(x1 + x2 + x3, yStretch(I, y4, stretchFactor, Ny, configuration))
    helperPoints = [hpoint1, hpoint1, hpoint2, hpoint2]
    
    # Gradients/slope of each straight line
    s1 = (hpoint1.gety() - points[0].gety())/(hpoint1.getx() - points[0].getx())
    s2 = (hpoint2.gety() - hpoint1.gety())/(hpoint2.getx() - hpoint1.getx())
    s3 = (point6.gety() - hpoint2.gety())/(point6.getx() - hpoint2.getx())
    s = [s1, s2, s2, s3]
    
    # Loop that solves for each intermediate point
    for i in range(4):
    
        # beta = arc angle from vertical
        # delta = slope angle from horizontal
        # A = length from corner point to helper point
        b = beta[i]*math.pi/180
        if b == 0:
            points.append(helperPoints[i])
        else:    
            d = math.atan(s[i])
            A = y0[i] - helperPoints[i].gety()
            
            # Code block that finds the point position relative to the helper point - dx, dy
            if b == 0:
                dx = 0
                dy = 0
            elif b * d >= 0:
                alpha = math.pi/2 - abs(b) - abs(d)
                L = A*math.sin(abs(b))/math.sin(alpha)
                dx = L*math.cos(abs(d))
                dy = -L*math.sin(abs(d))
                
            elif b * d < 0:
                alpha = math.pi/2 - abs(b) + abs(d)
                L = A*math.sin(abs(b))/math.sin(alpha)
                dx = L*math.cos(abs(d))
                dy = L*math.sin(abs(d))
           
            if b > 0:
                dx = -dx
            
            # Position of point    
            x = x0[i] + dx
            y = y0[i] - A + dy
            
            # added to array
            points.append(point(x, y))
        
    # Adds final (outlet) point to array
    points.append(point6)
    
    return points


def BuildTopLine(I, Ny, x, y, stretchFactor, configuration):
    '''
    Function to create the top grid line. Modified version of buildLine function that has no polynomial interpolation
    
    Input:
    - current interation
    - total iterations (y)
    - grid geometry
    - stretch factor
    
    Output:
    - X, Y coordinates
    '''
    
    # Number of data points
    Nx = 1000
    
    # Find the start/end points - arc angles set to zero so pts 2&3 and 4&5 are the same
    points = getPoints(I, Ny, x, y, [0, 0, 0, 0], stretchFactor, configuration)
    Nxi = allocateNx(Nx, points)
    
    # Line gradiets
    s1 = (points[1].gety() - points[0].gety())/(points[1].getx() - points[0].getx())
    s2 = (points[3].gety() - points[2].gety())/(points[3].getx() - points[2].getx())
    s3 = (points[5].gety() - points[4].gety())/(points[5].getx() - points[4].getx())
    grads = [s1, s1, s2, s2, s3, s3]
    
    # Points are allocated gradients
    for i in range(len(grads)):
        points[i].setGrad(grads[i])
        
    # Build coordinates of straight lines
    xlin1 = np.linspace(points[0].getx(), points[1].getx(), Nxi[0] + 1)
    ylin1 = np.linspace(points[0].gety(), points[1].gety(), Nxi[0] + 1)
    
    xlin3 = np.linspace(points[2].getx(), points[3].getx(), Nxi[2])
    ylin3 = np.linspace(points[2].gety(), points[3].gety(), Nxi[2])
    
    xlin5 = np.linspace(points[4].getx(), points[5].getx(), Nxi[4] + 1)
    ylin5 = np.linspace(points[4].gety(), points[5].gety(), Nxi[4] + 1)
    
    # Coordinate cutoff
    xlin1 = xlin1[:-1]
    ylin1 = ylin1[:-1]
    xlin5 = xlin5[1:]
    ylin5 = ylin5[1:]
    
    # Join coordinate arrays
    xfull = np.concatenate((xlin1, xlin3, xlin5), axis=0)
    yfull = np.concatenate((ylin1, ylin3, ylin5), axis=0)
    
    return xfull, yfull

    
def buildLine(I, Ny, x, y, beta, stretchFactor, configuration):
    '''
    Function that builds each streamwise gridline
    
    Straight --> polynimial --> straight --> polynomial --> straight
    
    Input:
    - I, current iteration
    - Ny, total number of y grid points
    - Grid geometry (x: section lengths, y: corner heights)
    - interpolation arc angles in degrees
    - stretch factor in y direction
    
    Output:
    - X, Y coordiantes
    
    '''
    # Number of data points on line
    Nx = 10000
    
    # Finds the start/end/intermediate points
    points = getPoints(I, Ny, x, y, beta, stretchFactor, configuration)
    
    # Allocates data points to each line section
    Nxi = allocateNx(Nx, points)
    
    # gradients of straight line sections
    s1 = (points[1].gety() - points[0].gety())/(points[1].getx() - points[0].getx())
    s2 = (points[3].gety() - points[2].gety())/(points[3].getx() - points[2].getx())
    s3 = (points[5].gety() - points[4].gety())/(points[5].getx() - points[4].getx())
    grads = [s1, s1, s2, s2, s3, s3]
    
    # Allocates points with gradients
    for i in range(len(grads)):
        points[i].setGrad(grads[i])
    
    # Polynomial line sections
    if abs(beta[0]) == 0 and abs(beta [1]) == 0:
        xlin2 = points[1].getx()
        ylin2 = points[1].gety()
    else:    
        xlin2, ylin2 = buildPoly(Nxi[1], points[1], points[2])
    if abs(beta[2]) == 0 and abs(beta [3]) == 0:
        xlin4 = points[3].getx()
        ylin4 = points[3].gety()
    else:    
        xlin4, ylin4 = buildPoly(Nxi[3], points[3], points[4])
    
    # Straight line sections
    xlin1 = np.linspace(points[0].getx(), points[1].getx(), Nxi[0] + 1)
    ylin1 = np.linspace(points[0].gety(), points[1].gety(), Nxi[0] + 1)
    
    xlin3 = np.linspace(points[2].getx(), points[3].getx(), Nxi[2] + 2)
    ylin3 = np.linspace(points[2].gety(), points[3].gety(), Nxi[2] + 2)
    
    xlin5 = np.linspace(points[4].getx(), points[5].getx(), Nxi[4] + 1)
    ylin5 = np.linspace(points[4].gety(), points[5].gety(), Nxi[4] + 1)
    
    # Coordinate cutoff to avoid doubling up at intermediate points
    xlin1 = xlin1[:-1]
    ylin1 = ylin1[:-1]
    xlin3 = xlin3[1:-1]
    ylin3 = ylin3[1:-1]
    xlin5 = xlin5[1:]
    ylin5 = ylin5[1:]
    
    # Joints X and Y arrays
    xfull = np.concatenate((xlin1, xlin2, xlin3, xlin4, xlin5), axis=0)
    yfull = np.concatenate((ylin1, ylin2, ylin3, ylin4, ylin5), axis=0)
    
    return xfull, yfull


def gridSpacingX(Nx, xFrame):
    '''
    Function to calculate the distribution of gridspacing along the streamwise direction. Used to create higher grid densities around corner regions
    
    Input:
    - Nx, number of grid  points in x
    - xFrame spacing between inflow, corners and outflow
    
    Output
    - array of dx values
    '''
    gridType = "uniform"
    [x1, x2, x3] = xFrame
    L = sum(xFrame)
    
    # Uniform spacing
    if gridType == "uniform":
    
        dx = L/(Nx - 1.0)
        
        dxList = np.ones(Nx - 1)*dx
        
        
    # Jump spacing
    elif gridType == "jump":
        
        dxList = []
        Nx -= 1
        xCorner = 10
        densityRatio = 5
        
        dxCorner = (L + 2*(densityRatio)*xCorner)/(densityRatio * Nx)
        xCurrent = 0
        for i in range(Nx):
            if xCurrent >= x1 - 0.5*xCorner and xCurrent < x1 + 0.5*xCorner:
                dx = dxCorner
            elif xCurrent >= x1 + x2 - 0.5*xCorner and xCurrent < x1 + x2 + 0.5*xCorner:
                dx = dxCorner
            else:
                dx = 5*dxCorner
                
            dxList.append(dx)
            xCurrent += dx
            
    return list(dxList)
    

def convertCoordinates(x, y, dxList):
    '''
    Funcction that takes a streamwise grid line and a distribution of streamwise grid spacing (dx) and outputs matching coordinates
    
    Input:
    - x and y coordinates from buildline
    - dx distribution
    
    Output:
    - x and y coordinates to be used in the final grid output
    '''
    xgrid = [x[0]]
    ygrid = [y[0]]
    xCurrent = 0
    Nx = len(dxList) + 1
    
    for i in range(Nx - 1):
        xCurrent += dxList[i]
        yCurrent = np.interp(xCurrent, x, y)
        
        xgrid.append(xCurrent)
        ygrid.append(yCurrent)
    
    
    return xgrid, ygrid
    
    
def buildGrid(Nx, Ny, x, y, beta, stretchFactor, configuration):
    '''
    Main Function that builds the entire grid
    
    Input:
    - Nx, total number of x grid points
    - Ny, total number of y grid points
    - Grid geometry (x: section lengths, y: corner heights)
    - interpolation arc angles in degrees
    - stretch factor in y direction 
    
    Output:
    - yOutput & xOutput: full grid output arrays with halo points
    
    '''
    
    # sanitise input methods to correct for bad beta values
    #print beta
    beta = sanitiseInput(x, y, beta)
    #print beta
    [x1, x2, x3] = x
    [y1, y2, y3, y4] = y
    
    # Find the grid spacing array in x
    dxList = gridSpacingX(Nx, x)
    
    # Initialises outputarrays
    xOutput = []
    yOutput = []
    
    # Loop to build steam wise gridlines
    for i in range(Ny):
        # Try/except block to catch errors
        try:
            # Builds a line at each iteration and plots coordinates
            if beta[0] == 0:
                X, Y = BuildTopLine(i, Ny, x, y, stretchFactor, configuration)
            else:
                if i==Ny-1:
                    X, Y = BuildTopLine(i, Ny, x, y, stretchFactor, configuration)
                else:
                    X, Y = buildLine(i, Ny, x, y, beta, stretchFactor, configuration)
            X, Y = convertCoordinates(X, Y, dxList)
            
            xOutput.append(X)
            yOutput.append(Y)
            
        except:
            # Displays lines that are not built
            print "*** No build on line {} ***".format(i)
            xOutput.append(np.zeros(Nx))
            yOutput.append(np.zeros(Nx))
            pass
        
    # Output coordinates
    return xOutput, yOutput

def padding(var, nhaloleft, nhaloright, configuration, beta=0.0, l=100.0, axis=0):
    '''
    Extrapolates grid coordinates into the halo points
    '''
    nz, ny, nx = var.shape

    # z extrapolaton
    if axis==0:
        for n in range(nhaloleft):
            if beta == 0.0:   
                z = l*(-n-1)/(nz - 1.0)
            else:
                if configuration=='corner':
                    z = l * sinh(beta *(-n-1)/(nz - 1.0))/sinh(beta)
                else:
                    z = l*0.5*(1.0 - tanh(beta*(1.0 - 2.0*(-n-1)/(nz - 1.0)))/tanh(beta))

            padleft = z*np.ones((ny,nx))
            var = np.concatenate((padleft[np.newaxis,:,:], var), axis=axis)

        for n in range(nhaloright):
            if beta == 0.0:   
                z = l*(n+nz)/(nz - 1.0)
            else:
                if configuration=='corner':
                    z = l * sinh(beta *(n+nz)/(nz - 1.0))/sinh(beta)
                else:
                    z = l*0.5*(1.0 - tanh(beta*(1.0 - 2.0*(n+nz)/(nz - 1.0)))/tanh(beta))

            padright = z*np.ones((ny,nx))
            var = np.concatenate((var, padright[np.newaxis,:,:]), axis=axis)

        padleft = np.repeat(var[:,np.newaxis,0,:], nhaloleft, axis=1)
        padright = np.repeat(var[:,np.newaxis,-1,:], nhaloright, axis=1)
        var = np.concatenate((padleft, var, padright), axis=1)

        padleft = np.repeat(var[:,:,np.newaxis,0], nhaloleft, axis=2)
        padright = np.repeat(var[:,:,np.newaxis,-1], nhaloright, axis=2)
        var = np.concatenate((padleft, var, padright), axis=2)

    # y extrapolaton
    elif axis==1:
        for n in range(nhaloleft):
            if beta == 0.0:   
                y = l*(-n-1)/(ny - 1.0)
            else:
                if configuration in ["full", "bottom shock", "channel shock", "channel"]:
                    y = l*0.5*(1.0 - tanh(beta*(1.0 - 2.0*(-n-1)/(ny - 1.0)))/tanh(beta))
                else:
                    y = l * sinh(beta *(-n-1)/(ny - 1.0))/sinh(beta)

            padleft = y*np.ones((nz,nx))
            var = np.concatenate((padleft[:,np.newaxis,:], var), axis=axis)

        for n in range(nhaloright):
            if beta == 0.0:   
                y = l*(n+ny)/(ny - 1.0)
            else:
                if configuration in ["full", "bottom shock", "channel shock", "channel"]:
                    y = l*0.5*(1.0 - tanh(beta*(1.0 - 2.0*(n+ny)/(ny - 1.0)))/tanh(beta))
                else:
                    y = l * sinh(beta *(n+ny)/(ny - 1.0))/sinh(beta)

            padright = y*np.ones((nz,nx))
            var = np.concatenate((var, padright[:,np.newaxis,:]), axis=axis)

        padleft = np.repeat(var[np.newaxis,0,:,:], nhaloleft, axis=0)
        padright = np.repeat(var[np.newaxis,-1,:,:], nhaloright, axis=0)
        var = np.concatenate((padleft, var, padright), axis=0)

        padleft = np.repeat(var[:,:,np.newaxis,0], nhaloleft, axis=2)
        padright = np.repeat(var[:,:,np.newaxis,-1], nhaloright, axis=2)
        var = np.concatenate((padleft, var, padright), axis=2)

    # x extrapolaton
    elif axis==2:
        dx = var[:,:,1] - var[:,:,0]
        for n in range(nhaloleft):
            padleft = var[:,:,0] - dx
            var = np.concatenate((padleft[:,:,np.newaxis], var), axis=axis)

        dx = var[:,:,-1] - var[:,:,-2]
        for n in range(nhaloright):
            padright = var[:,:,-1] + dx
            var = np.concatenate((var, padright[:,:,np.newaxis]), axis=axis)

        padleft = np.repeat(var[np.newaxis,0,:,:], nhaloleft, axis=0)
        padright = np.repeat(var[np.newaxis,-1,:,:], nhaloright, axis=0)
        var = np.concatenate((padleft, var, padright), axis=0)

        padleft = np.repeat(var[:,np.newaxis,0,:], nhaloleft, axis=1)
        padright = np.repeat(var[:,np.newaxis,-1,:], nhaloright, axis=1)
        var = np.concatenate((padleft, var, padright), axis=1)

    else:
        print "return valid axis number: {0,1,2}"
        return

    return var

def zeropadding(var, nhaloleft, nhaloright):
    '''
    Pads grid arrays with zeros in the halo points
    '''

    nz, ny, nx = var.shape

    padleft = np.zeros((nhaloleft, ny, nx))
    padright = np.zeros((nhaloright, ny, nx))
    var = np.concatenate((padleft, var, padright), axis=0)

    padleft = np.zeros((nz + nhaloleft + nhaloright, nhaloleft, nx))
    padright = np.zeros((nz + nhaloleft + nhaloright, nhaloright, nx))
    var = np.concatenate((padleft, var, padright), axis=1)

    padleft = np.zeros((nz + nhaloleft + nhaloright, ny + nhaloleft + nhaloright, nhaloleft))
    padright = np.zeros((nz + nhaloleft + nhaloright, ny + nhaloleft + nhaloright, nhaloright))
    var = np.concatenate((padleft, var, padright), axis=2)

    return var


def generate(theta, x1, x2, x3, y1, z1, nx, ny, nz, by, bz, configuration="bottom"):
    '''
    Main function, processes inputs, generate x-z grid plane, adds spanwise points, adds halo points
    '''

    theta = [0, theta, 0]
    lx = [x1, x2, x3]
    ly = [y1, 0, 0, 0]

    for i in range(1,4):
        ly[i] = ly[i-1] - lx[i-1]*math.tan(theta[i-1]*math.pi/180)

    # Beta denotes the arc angles of the polynomial regions. Arcs are centered on the middle corner points and require a start and end beta value. Values are in degrees; positive = clockwise.
    b = 45
    beta = [b, -b, b, -b]

    # Generate x-y grid plane
    x, y = buildGrid(nx, ny, lx, ly, beta, by, configuration)

    # Get spanwise points based on stretching configuration
    if bz == 0.0:   
        z = np.linspace(0.0,z1,nz)
    else:
        if configuration=='corner':
            z = z1 * sinh(bz*np.linspace(0,nz-1,nz)/(nz - 1))/sinh(bz)
        else:
            z = z1*0.5*(1.0 - tanh(bz*(1.0 - 2.0*np.linspace(0,nz-1,nz)/(nz - 1.0)))/tanh(bz))
    
    # Cast x,y,z data into 3D arrays
    x = np.repeat(np.array(x)[np.newaxis,:,:], nz, axis=0)
    y = np.repeat(np.array(y)[np.newaxis,:,:], nz, axis=0)
    z = np.repeat(np.repeat(np.array(z)[:,np.newaxis], ny, axis=1)[:,:,np.newaxis], nx, axis=2)

    nhaloleft, nhaloright = 5,5

    # Add extrapolation halo points
    # x = padding(x, nhaloleft, nhaloright, configuration, axis=2)
    # y = padding(y, nhaloleft, nhaloright, configuration, beta=by, l=y1, axis=1)
    # z = padding(z, nhaloleft, nhaloright, configuration, beta=bz, l=z1, axis=0)

    # nhaloleft, nhaloright = 2,2

    # # Add zero points
    # x = zeropadding(x, nhaloleft, nhaloright)
    # y = zeropadding(y, nhaloleft, nhaloright)
    # z = zeropadding(z, nhaloleft, nhaloright)

    return x, y, z


def visualisegrid(x, y, k=0):
    '''
    Function to plot generated grid
    '''
    nz, ny, nx = x.shape

    x = x[k+5,5:ny-5,5:nx-5]
    y = y[k+5,5:ny-5,5:nx-5]

    ny, nx = x.shape

    with PdfPages('grid.pdf') as pdf1:
        fig = plt.figure()
        for j in range(ny):
            plt.plot(x[j,:], y[j,:], 'b-',linewidth=0.01)

        for i in range(nx):
            plt.plot(x[:,i], y[:,i], 'b-',linewidth=0.01)

        plt.xlabel(r"x")
        plt.ylabel(r"y")
        plt.gca().set_aspect('equal', adjustable='box')
        pdf1.savefig(bbox_inches='tight')
        plt.close()

        fig = plt.figure()
        dy = np.zeros((ny-1,nx))
        for i in range(nx):
            for j in range(ny-1):
                dy[j,i] = y[j+1,i] - y[j,i]
                if dy[j,i] < 0.0:
                    print "Negative dy found: [{},{}]".format(i,j)

            if i&1==0:
                plt.plot(y[1:,i], dy[:,i], linewidth=0.2)

        plt.xlabel(r"y")
        plt.ylabel(r"dy")
        pdf1.savefig(bbox_inches='tight')
        plt.close()


'''
Helper Classes
'''

class point(object):
    '''
    Simple coordinate object with x, y values
    
    Optionally, a gradient value can be set
    '''
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def getx(self):
        return self.x
        
    def gety(self):
        return self.y
        
    def setGrad(self, grad):
        self.grad = grad   
        
    def getGrad(self):
        return self.grad


'''
Function Calls
'''    


'''
Grid geometry:
 _________________ _________________ _________________                 
|                 |                 |                 |
|                 |                 |                 |
|                 |                 |                 | 
|y[0]             |y[1]             |y[2]             |y[3]
|                 |                 |                 | 
|                 |                 |                 | 
|                 |                 |                 | 
|_________________|_________________|_________________|
        x[0]              x[1]             x[2]
'''
if __name__ == '__main__':
    theta = 20.0
    x1 = 75.0
    x2 = 100.0
    x3 = 125.0
    y1 = 100
    z1 = 100
    nx = 432
    ny = 300
    nz = 120
    by = 1.9
    bz = 0.0

    x, y, z = generate(theta, x1, x2, x3, y1, z1, nx, ny, nz, by, bz, configuration="bottom shock")

    gf = h5py.File('gridtest.h5', 'w')
        
    gf.create_dataset('x0', data=x)
    gf.create_dataset('x1', data=y)
    gf.create_dataset('x2', data=z)
    gf.close()

    visualisegrid(x,y)




