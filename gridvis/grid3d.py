from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

import grid

#theta = 9.0
x1 = 150.0
x2 = 100.0
x3 = 300.0
y1 = 100
z1 = 100
nx = 432
ny = 300
nz = 120
#by = 1.9
#bz = 1.9

def gridimage(theta, by, bz, configuration, filename, crop_params):

	x, y, z = grid.generate(theta, x1, x2, x3, y1, z1, nx, ny, nz, by, bz, configuration=configuration)
	n = x.shape

	frontx = x[:,:,0]
	fronty = y[:,:,0]
	frontz = z[:,:,0]

	topx = x[:,n[1]-1,:]
	topy = y[:,n[1]-1,:]
	topz = z[:,n[1]-1,:]

	sidex = x[n[0]-1,:,:]
	sidey = y[n[0]-1,:,:]
	sidez = z[n[0]-1,:,:]

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d', aspect='equal')
	ax.plot_wireframe(frontz, frontx, fronty, linewidth=0.25, color='k', rstride=10, cstride=10)
	ax.plot_wireframe(topz, topx, topy, linewidth=0.25, color='k', rstride=10, cstride=10)
	ax.plot_wireframe(sidez, sidex, sidey, linewidth=0.25, color='k', rstride=10, cstride=10)

	max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max()
	Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(x.max()+x.min())
	Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(y.max()+y.min())
	Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(z.max()+z.min())
	# Comment or uncomment following both lines to test the fake bounding box:

	for xb, yb, zb in zip(Xb, Yb, Zb):
	   ax.plot([xb], [yb], [zb], 'w')

	# ax.plot_wireframe(z[:,:,nx[0] - 1], x[:,:,nx[0] - 1], y[:,:,nx[0] - 1], linewidth=0.5, color='k', rstride=10, cstride=10)
	# ax.plot_wireframe(z[nx[1]-1,:,:], x[nx[1]-1,:,:], y[nx[1]-1,:,:], linewidth=0.5, color='k', rstride=10, cstride=10)
	#ax = plt.gca(projection='3d') 
	ax._axis3don = False

	plt.savefig("{}".format(filename), bbox_inches='tight', dpi=1000)

	px_h = 3902
	px_w = 2829

	coords = (int(crop_params[0]*px_w), int(crop_params[1]*px_h), int(crop_params[2]*px_w), int(crop_params[3]*px_h))

	image_obj = Image.open("{}".format(filename))
	cropped_image = image_obj.crop(coords)
	cropped_image.save("{}".format(filename))

gridimage(0.0, 2.5, 0.0, "bottom", "bottom.png", [0.25, 0.28, 0.52, 0.55])
gridimage(0.0, 1.9, 1.9, "channel", "channel.png", [0.25, 0.28, 0.52, 0.55])
gridimage(9.0, 1.9, 0.0, "bottom shock", "bottomshock.png", [0.25, 0.3, 0.52, 0.55])
gridimage(9.0, 1.9, 1.9, "channel shock", "channelshock.png", [0.25, 0.3, 0.52, 0.55])