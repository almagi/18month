\chapter{Current \& Future Work}
	\label{chap:future}
	
	\section{Current Work}
		\label{sec:current}
	
		The work currently being undertaken is to validate the turbulent inflow generation in an OpenSBLI simulation consisting of a simple flat plate, zero-pressure gradient boundary layer. This is done by applying the method as a Dirichlet boundary condition at the inflow plane and looking at various aspects of the boundary layer, with particular interest on the time-averaged RMS profiles as well as the streamwise variation of skin friction. These parameters will provide the information of the required relaxation distance needed to provide an accurate turbulent boundary layer, free of any transient effects of the correlation method.\\
		
		\noindent Application of the method has proved difficult for a number of reasons. One issue is the matter of generating random numbers. The prototype code benefited from the fact that accurate, uncorrelated random numbers could be generated easily by using a pseudorandom number generator (PRNG) from one of Python's boilerplate libraries. The computational performance of the prototype was unimportant since only 2D planes of data were generated at each iteration. The implementation of the method on OpenSBLI, however, was run on a CUDA (GPU architecture) version of C and therefore a PRNG, such as those in  Python libraries, would not be practical due to the need to share a global PRNG state value between parallel processors. Using the in-built PRNG in CUDA was found to add a serious cost in performance - increasing the run-time by up to a factor of 100.\\
		
		\noindent A solution to this was found by hand-writing a PRNG in C which would produce three (one for each velocity component) unique random numbers for each ($y$, $z$) location and at each time step. The results of this were mixed: on the one hand, the computational performance was excellent, with the method contributing no noticeable slow down of the code. On the other, the random numbers exhibited strong cyclical patterns and therefore created very obvious repeated structures at the inflow plane. The results of this are clear from figure \ref{fig:random-issues} which shows contours of density at three $y-z$ planes at various stream wise positions. The patterns in the inflow plane are striking, and, while these structures decay, the remnants of them are clear even at a downstream station of $x = 10\delta_0$. Generally, this feature should be avoided since it has the potential to add artificial structures and frequencies to the downstream flow field. One of the current tasks is to implement a better method of generating random numbers without adding too much computational cost.\\
		
		\noindent Since OpenSBlI is relatively new software, developing new features for it means coming across problems and bugs is a common occurrence. Such issues include, but are not limited to:
		
		\begin{itemize}
			\item Compatibility between architectures (CUDA and CUDA-MPI)
			\item Version control of external libraries
			\item Issues reading from the data files used to store the mean profile data
			\item Correct implementation of the correlation method using OPS data objects in place of data arrays
			\item Difficulty in debugging (e.g. print statements)
			\item Overhead cost of running individual simulations (code generation, compilation, copying files etc.)
		\end{itemize}
		
		\noindent It is expected that the remaining issues will be ironed out with the next two weeks, allowing for statistics of the flat plate boundary layer to be gathered over a long simulation period. The time-averaged RMS profiles can then be compared against the target data as in section \ref{sec:initial-testing}. This will allow for the approximate relaxation distance to be found and therefore revealing the earliest streamwise position where a SWBLI can be imposed.\\
		
		\noindent Once the inflow method is fully validated, it must be extended to generate the sidewall boundary layers as well as the bottom wall. With the numerical grid already defined, all the required steps to begin the work on SWBLI will be complete.\\
		
				
		\begin{figure}[H]
			\centering
			\begin{widepage}
				\begin{subfigure}[b]{0.5\textwidth}
					\centering
					\includegraphics[width=1.0\textwidth]{density-contour-inflow.png}
					\caption{Instantaneous flow field at $x = 0$}
					\label{fig:random-issues-inflow}
				\end{subfigure}
				~
				\begin{subfigure}[b]{0.5\textwidth}
					\centering
					\includegraphics[width=1.0\textwidth]{density-contour-mid.png}
					\caption{Instantaneous flow field at $x = 5\delta_0$}
					\label{fig:random-issues-mid}
				\end{subfigure}
				~
			\end{widepage}
			\begin{subfigure}[b]{0.6\textwidth}
				\centering
				\includegraphics[width=1.0\textwidth]{density-contour-end.png}
				\caption{Instantaneous flow field at $x = 10\delta_0$}
				\label{fig:random-issues-end}
			\end{subfigure}
			\caption[Density contour plots showing the flow structures resulting from the PRNG]{Density contour plots of the instantaneous flow field in the y-z plane at various stream wise locations. Simulation was conducted with an input Mach and Reynolds numbers of 2.7 and 3350. The domain size was $N_x, N_y, N_z = 256, 400, 125$ and the domain lengths (in $\delta_0$ were $L_x, L_y, L_z = 10, 2, 2$. The plots show the decay of the flow structures resulting from the PRNG.}
			\label{fig:random-issues}
		\end{figure}
		
		
	\section{Future Work}
		\label{sec:future}
		
		The next stage of the validation process is the application of the inflow method to a SWBLI case. This will provide in important demonstration of the application of OpenSBLI to problems shock waves and turbulent flow. It was chosen that this would be conducted with a replication of one of the simulation cases from Wang et al. \cite{wang2015} which was discussed in detail in sections \ref{sec:swept} and \ref{sec:corner}. The reasoning for this choice is as follows:
		
		\begin{itemize}
			\item The study was conducted within the research group at the University of Southampton on the precursor solver to OpenSBLI - therefore meaning instant access to the simulation data and source codes
			\item One of the main considerations of the work was on sidewall influences which strongly matches that of this project, meaning the results can be more easily built upon following the validation process
		\end{itemize}
		
		\noindent Rerunning this previous test case and comparing how well the flow field is replicated will allow for an examination of the shock-capturing methods employed by OpenSBLI. It is expected that a targeted essentially non-oscillatory (TENO) formulation with implicit-LES will be used which can be compared to the TVD/LES formulation used by Wang et al. \\
		
		
		\noindent Chapter \ref{chap:lit} provided a review of historical and contemporary literature and the following conclusions were made regarding the deficit of knowledge within the field of reflected SWBLI:
		
		\begin{itemize}
			\item The general paucity of work done on irregular SWBLI/Mach reflections, especially concerning numerical studies and studies considering the influence of sidewalls
			\item The lack of a prediction technique for the sizes of the interaction and separation length that accounts for the confinement of the test area
		\end{itemize}
		
		\noindent These conclusions provide an opportunity for novel research following on from the validation processes. With SWBLI cases already run, and provided the numerical formulation is suitable, new cases can be designed which will consider the various impacts of increasing the strength of the SWBLI. It is expected that this will be achieved initially by increasing the flow deflection angle of the shock generator but a change in the Mach number may also be considered. To include the influence of sidewalls, comparisons can be made between different working section aspect ratios and with span-periodic cases. By considering the influence one or two key parameters it is hoped that a valuable insight into this uncharted area of research can be made.\\
		
		\noindent Figure \ref{fig:gantt} provides a plan for the next 9 months of the research project. The next assessment deadline is March 25th 2019 which is expected to incorporate work from the validation phases as well as work on the strong interaction cases.
		
	
		\begin{figure}[h]
			\makebox[\textwidth][c]{
			\begin{ganttchart}[
				today=8,
				vgrid=true,
				hgrid=true,
				progress=today,
				bar/.append style={fill=grey},
				bar incomplete/.append style={fill=none},
				group incomplete/.append style={draw=none,fill=black},
				milestone incomplete/.append style={fill=black},
				progress label text = ''
				]{1}{24}
				\gantttitle{Calendar Year}{24} \\
				
			    \gantttitle{Apr}{2}
			    \gantttitle{May}{2}
			    \gantttitle{Jun}{2}
			    \gantttitle{Jul}{2}
			    \gantttitle{Aug}{2}
			    \gantttitle{Sep}{2}
			    \gantttitle{Oct}{2}
			    \gantttitle{Nov}{2}
			    \gantttitle{Dec}{2}
			    \gantttitle{Jan}{2}
			    \gantttitle{Feb}{2}
			    \gantttitle{Mar}{2}\\
				
				\ganttgroup{9 Month Report}{1}{6}\\
				\ganttbar{Lit. Review}{1}{6}\\
				\ganttbar{Writing Phase}{5}{6}\\
				\ganttmilestone{Deadline}{6}\\
				
				\ganttgroup{Inflow Validation}{1}{8}\\
				\ganttbar{Prototype Method}{1}{3}\\
				\ganttbar{OpenSBLI Testing}{4}{8}\\
				
				\ganttgroup{SWBLI Validation}{8}{10}\\
				\ganttbar{Wang \cite{wang2015} Case Validation}{8}{10}\\
				\ganttbar{Assessment of Numerical Methods}{8}{10}\\
				\ganttmilestone{UK Fluids Meeting, Manchester}{10}\\
				
				\ganttgroup[group label font=\color{blue}]{\textbf{Strong Interaction Cases}}{11}{20}\\
				\ganttbar[bar label font=\color{blue}]{Set-up \& Running Simulations}{11}{16}\\
				\ganttbar[bar label font=\color{blue}]{Post-Process \& Analyse Results}{13}{20}\\
				
				\ganttgroup{HPC Allocations}{3}{20}\\
				\ganttbar{Archer Tier-2 (\textit{via CSD3)}}{3}{8}
				\ganttbar{}{9}{14}
				\ganttbar{}{15}{20}\\
				\ganttbar{Archer Tier-1 (\textit{via UKTC)}}{7}{18}\\
				
				\ganttgroup{18 Month Report}{21}{24}\\
				\ganttbar{Writing Phase}{21}{24}\\
				\ganttmilestone{Deadline}{24}
				
			\end{ganttchart}
			}
			\caption[Research plan for the next 9 months]{Gantt chart showing the current progress and work plan leading up to the 18 month report in 9 months time. Highlighted in blue is the core research within this period. The allocation periods for the HPC resources are shown.}
			\label{fig:gantt}
		\end{figure}