%%%%%%%%%%%%%%%%% Alex Gillespie
%%%%%%%%%%%%%%%%% PhD Transfer report
%%%%%%%%%%%%%%%%% simulations of strong shock wave boundary layer interactions


%%%%%%%%%%%%%%% Preamble %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[a4paper]{report}
\title{18month}

% Encoding
\usepackage[utf8]{inputenc}

% Language
\usepackage[english]{babel}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage[autostyle, english = american]{csquotes}
\MakeOuterQuote{"}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{xfrac}
\usepackage{units}
\usepackage{mathtools}

% Figures
%\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{epstopdf}
\usepackage{pgfgantt}

% TikZ
%\usepackage{tikz}
%\usetikzlibrary{shapes,arrows}

% Tables
\usepackage{booktabs}

% Bibliography
\usepackage[numbers,sort&compress]{natbib}
\renewcommand{\bibsection}{\chapter*{References}}
\usepackage{url}

% Appendix
\usepackage{appendix}

% Nomenclature
\usepackage{nomencl} 
\makenomenclature
\usepackage{etoolbox}
\renewcommand{\nomname}{List of Symbols}
\renewcommand\nomgroup[1]{%
\ifthenelse{\equal{#1}{A}}{\vspace{0.5cm}\item[\textbf{Latin Symbols}]}{%
	\ifthenelse{\equal{#1}{B}}{\vspace{0.5cm}\item[\textbf{Greek Symbols}]}{%
		\ifthenelse{\equal{#1}{C}}{\vspace{0.5cm}\item[\textbf{Abbreviations}]}{
			\ifthenelse{\equal{#1}{D}}{\vspace{0.5cm}\item[\textbf{Subscripts}]}{
				\ifthenelse{\equal{#1}{E}}{\vspace{0.5cm}\item[\textbf{Superscripts}]}
				}
			}
		}
	}
}

% colours
\usepackage{xcolor}
\definecolor{grey}{gray}{0.6}

% Page Layout
\usepackage{a4wide}
\usepackage{multirow}
\usepackage{ragged2e}
\usepackage{titlesec}
\usepackage{multicol}
\usepackage{changepage}
\usepackage[export]{adjustbox}[2011/08/13]
\usepackage{fullpage}
\usepackage{comment}
\usepackage{lscape}

\newlength{\offsetpage}
\setlength{\offsetpage}{1.0cm}
\newenvironment{widepage}{\begin{adjustwidth}{-\offsetpage}{-\offsetpage}%
    \addtolength{\textwidth}{2\offsetpage}}%
{\end{adjustwidth}}

\titleformat{\chapter}[block]
{\normalfont\huge\bfseries}{\thechapter.}{1em}{\Huge}
\titlespacing*{\chapter}{0pt}{-19pt}{0pt}



\begin{document}


%%%%%%%%%%%%%%%%%% Title Page %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{titlepage}
	\begin{center}
		
		\noindent
		\vspace{1cm}
%		\large
%		{\it Aerodynamics \& Flight Mechanics\\
%		\it Faculty Engineering \& Physical Sciences\\
%		\it University of Southampton\\
%		}
		
%		\vspace{0.5cm}
		\begin{figure}[H] 
			\centering
			\includegraphics[draft=false,width=0.2\textwidth]{black.png}
		\end{figure}
		
		\vspace{.5cm}
		\Huge{{\bf Simulations of Strong Shock Wave/Boundary Layer Interactions}} \\ \vspace{0.5cm} \huge{Transfer Report}
		\vspace{.5cm}
	\end{center}
	
	\vspace{2cm}
	\centering
	\large
	%Author:\\
	\huge{\bf Alexander Gillespie}\\
	\vspace{0.25cm}
	\Large{\it Aerodynamics \& Flight Mechanics}\\
	\Large{\it University of Southampton}\\
	
%	\vspace{1cm}
%	\large
%	Supervisor:\\
%	\Large{Prof. Neil Sandham}\\
	
	\vspace{2cm}
	In Partial Fulfilment for the Degree of\\
	\Large{\textit{Doctor of Philosophy}}
	
	\vspace{3cm}
	\Large
	\today
\end{titlepage}

%%%%%%%%%%%%%%%%%%%%%%% Pretext %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage
\pagenumbering{gobble}

%%%%%%%%%%%%%%%%%%%% Abstract

\clearpage\mbox{}\clearpage

\pagenumbering{roman}
\null\vfil
\begin{center}{\Large\bf Abstract \par}\end{center}
\addcontentsline{toc}{chapter}{Abstract}
\noindent This report provides a summary of the work conducted over the first 18 months of a doctoral project focusing strong shock wave/boundary layer interactions (SBLI) with the application of sidewalls. An up to date literature study is provided as well as a detailed description of the computational set up. The results of a modified turbulent boundary layer generation method are presented, followed by results of exploratory and validation studies of SBLIs. The exploratory studies were able to demonstrate the effects of increasing the strength of the incident shock on the interaction region within highly confined ducts. Stronger shocks are able to increase the overall size of the interaction, and, in the most extreme cases, create separation bubbles which dominate the domain. The application of sidewalls was shown to reduce the size of the interaction due to the fact that the incident shock is blurred over a larger distance. The validation work is involved with replicating cases from a previously published SBLI study. The results are reasonably well-replicated, except for the separation length of the interaction, which is up to $100\%$ larger when compared to the reference data. Finally, the research plan for the next 18 months of the project is outlined.

\newpage
\clearpage\mbox{}\clearpage
%%%%%%%%%%%%%%%%%%%% Acknowledgments

\null\vfil
\begin{center}{\Large\bf Acknowledgements \par}\end{center}
\addcontentsline{toc}{chapter}{Acknowledgements}

\noindent I would like to express thanks to my supervisor, Prof. Neil Sandham, for his advice and guidance throughout the past 18 months. I must also acknowledge the financial support of MBDA (UK), without whom this project would not have happened. Lastly, I would like to acknowledge both the UK Turbulence Consortium (UKTC) and the Cambridge Service for Data Driven Discovery (CSD3) for providing computational time for this project.
\newpage
\clearpage\mbox{}\clearpage
%%%%%%%%%%%%%%%%%%%% Declaration of Authorship

\addcontentsline{toc}{chapter}{Declaration of Authorship}
%\thispagestyle{plain}
\null\vfil
%\vskip 60\p@
\begin{center}{\Large\bf Declaration of Authorship \par}\end{center}
{\normalsize I, Alexander Gillespie, declare that the report entitled {\it Simulations of Strong Shock Wave/Boundary Layer Interactions} 
and the work presented in the thesis are both my own, and have been generated by me as the result of my own original research. I confirm that:

\begin{itemize}
\item this work was done wholly or mainly while in candidature for a research degree
at this University;
\item where any part of this thesis has previously been submitted for a degree or any
other qualification at this University or any other institution, this has been clearly
stated;
\item where I have consulted the published work of others, this is always clearly
attributed;
\item where I have quoted from the work of others, the source is always given. With
the exception of such quotations, this thesis is entirely my own work;
\item I have acknowledged all main sources of help;
\item where the thesis is based on work done by myself jointly with others, I have
made clear exactly what was done by others and what I have contributed myself;
\item none of this work has been published before submission
\end{itemize}
\vspace{5.0mm}
Signed:.......................................................................................................................

\vspace{3.0mm}
\noindent Date:..........................................................................................................................}\newpage
\clearpage\mbox{}\clearpage



%%%%%%%%%%%%%%%%%%%% Contents, figures, tables
\tableofcontents
\newpage
\addcontentsline{toc}{chapter}{List of Figures}
\listoffigures

\newpage
\addcontentsline{toc}{chapter}{List of Tables}
\listoftables

%%%%%%%%%%%%%%%%%%%% Nomenclature


% Latin
\nomenclature[A]{$H$}{Shape factor, Height}
\nomenclature[A]{$M$}{Mach number}
\nomenclature[A]{$R$}{Specific gas constant, Reynolds stress tensor}
\nomenclature[A]{$r$}{Normally-distributed random number}
\nomenclature[A]{$p$}{Pressure}
\nomenclature[A]{$T$}{Temperature}
\nomenclature[A]{$u, v, w$}{Velocity components}
\nomenclature[A]{$a$}{Local speed of sound, fluctuation amplitude matrix}
\nomenclature[A]{$x, y, z$}{Position components}
\nomenclature[A]{$i, j, k$}{Index values}
\nomenclature[A]{$q$}{Dynamic pressure}
\nomenclature[A]{$E$}{Energy}
\nomenclature[A]{$Re$}{Reynolds number}
\nomenclature[A]{$Pr$}{Prandtl number}
\nomenclature[A]{$L, l$}{Length}
\nomenclature[A]{$W$}{Width}
\nomenclature[A]{$I$}{Integral length scale}
\nomenclature[A]{$u_{\tau}$}{Friction velocity}
\nomenclature[A]{$u^*$}{Unscaled velocity fluctuations}
\nomenclature[A]{$t$}{Time}
\nomenclature[A]{$C$}{Sutherland constant}
\nomenclature[A]{$C_f$}{Skin friction}
% \nomenclature[A]{$$}{}


% Greek
\nomenclature[B]{$\delta$}{Boundary layer thickness, flow deflection angle}
\nomenclature[B]{$\delta_0$}{Inlet boundary layer thickness}
\nomenclature[B]{$\delta^*$}{Displacement thickness}
\nomenclature[B]{$\theta$}{Momentum thickness}
\nomenclature[B]{$\gamma$}{Heat capacity ratio}
\nomenclature[B]{$\rho$}{Density}
\nomenclature[B]{$\beta$}{Shock angle, grid stretching factor}
\nomenclature[B]{$\mu$}{Viscosity}
\nomenclature[B]{$\kappa$}{Von K{\'a}rm{\'a}n constant}
\nomenclature[B]{$\Phi$}{Shock capturing threshold parameter}
\nomenclature[B]{$\tau$}{Stress tensor}
\nomenclature[B]{$\phi, \psi$}{Intermediate correlation variables}
\nomenclature[B]{$\xi, \eta, \zeta$}{Curvilinear Metric terms}
\nomenclature[B]{$\omega$}{Vorticity}

% Abbreviations
\nomenclature[C]{$PIV$}{Particle image velocimetry}
\nomenclature[C]{$SBLI$}{Shock wave boundary layer interaction}
\nomenclature[C]{$ISBLI$}{Irregular shock wave boundary layer interaction}
\nomenclature[C]{$TBL$}{Turbulent boundary layer}
\nomenclature[C]{$CFD$}{Computational fluid dynamics}
\nomenclature[C]{$(W/T)ENO$}{(Weighted/Targeted) Essentially non-oscillatory}
\nomenclature[C]{$LES$}{Large eddy simulation}
\nomenclature[C]{$DNS$}{Direct numerical simulation}
\nomenclature[C]{$RANS$}{Reynolds-averaged Navier-Stokes}
\nomenclature[C]{$HPC$}{High performance computing}
\nomenclature[C]{$VCO$}{Virtual conical origin}
\nomenclature[C]{$RR$}{Regular reflection}
\nomenclature[C]{$MR$}{Mach reflection}
\nomenclature[C]{$CPU$}{Central processing unit}
\nomenclature[C]{$GPU$}{Graphics processing unit}
\nomenclature[C]{$OPS$}{Oxford parallel structured software}
\nomenclature[C]{$RMS$}{Root mean square}
\nomenclature[C]{$FTT$}{Flow through time}
\nomenclature[C]{$TVD$}{Total variation diminishing}
\nomenclature[C]{$MPI$}{Message passing interface}
\nomenclature[C]{$CUDA$}{Compute unified device architecture (Nvidia)}
\nomenclature[C]{$RK$}{Runge-Kutta}
\nomenclature[C]{$AR$}{Aspect ratio}
\nomenclature[C]{$BC$}{Boundary condition}
\nomenclature[C]{$CAD$}{Constant area duct}


% Subscripts
\nomenclature[D]{$\infty$}{Upstream value}
\nomenclature[D]{$e$}{Local freestream value}
\nomenclature[D]{$t, 0$}{Total/stagnation value}
\nomenclature[D]{$i, inc$}{Incompressible}
\nomenclature[D]{$c$}{Compressible}
\nomenclature[D]{$bl$}{Boundary layer}
\nomenclature[D]{$w, wall$}{Wall value}
\nomenclature[D]{$vd$}{Relating to the van Driest transform}
\nomenclature[D]{$sep$}{Separation}
\nomenclature[D]{$r$}{Dimensional reference value}
\nomenclature[D]{$i,j,k$}{indices}

% Superscripts
\nomenclature[E]{$'$}{Fluctuating component}
\nomenclature[E]{$+$}{Wall-unit}
% \nomenclature[D]{$$}{}

\newpage
\addcontentsline{toc}{chapter}{List of Symbols} \label{nomenclature}
\printnomenclature
\clearpage\mbox{}\clearpage

\clearpage
\setcounter{page}{1}
\pagenumbering{arabic}
%%%%%%%%%%%%%%%%% Chapter Includes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\nocite{*}
\include{introduction}
\include{literature}
\include{numerical}
\include{results}
\include{futurework}

%%%%%%%%%%%%%%%%% References, Appendices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\addcontentsline{toc}{chapter}{References}
\bibliography{references}
\bibliographystyle{abbrv}

\appendix





\end{document}









